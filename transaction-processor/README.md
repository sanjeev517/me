Description
-------------------------
As a part of the requirement this application calculates  relative account balance (positive or negative) for a given time frame for supplied transactions.

### Component overview :

    The main class of this application is  com.account.RelativeBalanceCalculator.
        -> It initialise data(data/TransactionDao) from src/main/resources/transactions.csv file.
        -> It allows user to supply accountId, from data and to-date through command line. 
        -> The core business logic has been implemented into the class service/AccountBalanceService.
            - relative account balance
            - number of transactions
No external lib has been used except Jackson for csv parsing and java bean domain initialization.

## Build and Run application

### Prerequisites
	Java 1.8
	Apache Maven (3x)

### Build
	mvn clean install
	This will produce fat executable jar in target folder named transaction-processor.jar
	
### Test case execution
	mvn test

### To run application from terminal
	java -jar target/transaction-processor.jar (ensure that application is already build using 'mvn clean install')

### Console input format
	accountId : ACC334455
	From(dd/MM/yyyy HH:mm:ss) : 20/10/2018 12:00:00
	to(dd/MM/yyyy HH:mm:ss) : 20/10/2018 19:00:00
	
	Relative balance for the period is :-25.0
	Number of transactions included is :1
![alt text](output.png?raw=true "Output")
	
	