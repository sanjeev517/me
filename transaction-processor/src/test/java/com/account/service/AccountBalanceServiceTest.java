package com.account.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.DoubleSummaryStatistics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.account.data.TransactionDao;
import com.account.model.Transaction;

public class AccountBalanceServiceTest {

    final File input = new File("src/main/resources/transactions.csv");
    private AccountBalanceService service;

    @Before
    public void init() throws FileNotFoundException{
      TransactionDao dao =  new TransactionDao(new FileInputStream(input));
      service = new AccountBalanceServiceImpl(dao);
    }


    @Test
    public void calculateBalance_and_expect_three_transactions(){
        String accountId =  "ACC778899";
        String from = "20/10/2018 12:00:00";
        String to = "21/10/2018 19:00:00";

        DoubleSummaryStatistics statistics = service.calculateRelativeBalance(accountId, LocalDateTime.parse(from, Transaction.FORMATTER), LocalDateTime.parse(to, Transaction.FORMATTER));
        Assert.assertEquals(3, statistics.getCount());
        Assert.assertEquals(37.25, statistics.getSum(), 0);
    }

    @Test
    public void calculateBalance_with_sample_input_test(){
        String accountId =  "ACC334455";
        String from = "20/10/2018 12:00:00";
        String to = "20/10/2018 20:00:00";

        DoubleSummaryStatistics statistics = service.calculateRelativeBalance(accountId, LocalDateTime.parse(from, Transaction.FORMATTER), LocalDateTime.parse(to, Transaction.FORMATTER));
        Assert.assertEquals(1, statistics.getCount());
        Assert.assertEquals(-25.0, statistics.getSum(), 0);
    }

    @Test
    public void calculateBalanceWithReversedTransactionOutDateRange(){
        String accountId =  "ACC334455";
        String from = "20/10/2018 12:00:00";
        String to = "20/10/2018 19:00:00";

        DoubleSummaryStatistics statistics = service.calculateRelativeBalance(accountId, LocalDateTime.parse(from, Transaction.FORMATTER), LocalDateTime.parse(to, Transaction.FORMATTER));
        Assert.assertEquals(1, statistics.getCount());
        Assert.assertEquals(-25.0, statistics.getSum(), 0);
    }

}
