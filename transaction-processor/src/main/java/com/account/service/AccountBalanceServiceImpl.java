package com.account.service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.DoubleSummaryStatistics;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.account.data.TransactionDao;
import com.account.model.Transaction;
import com.account.model.Transaction.TransactionType;

public class AccountBalanceServiceImpl implements AccountBalanceService {
	

	private TransactionDao transactionDao;

	public AccountBalanceServiceImpl(TransactionDao trxDao) {
		this.transactionDao = trxDao;
		
	}
	//LocalDateTime from = Transaction.toLocaleDateTime(fromDate);
//    LocalDateTime to = Transaction.toLocaleDateTime(toDate);

    @Override
    public DoubleSummaryStatistics calculateRelativeBalance(String accountId, LocalDateTime from , LocalDateTime to ) {
    	List<Transaction> transactions = transactionDao.getAllTransactions();
        Collections.reverse(transactions);
       //select accounts by accountId and prepare array of transactions to be excluded/reversed.
        Set<String> reversedTransactions = new HashSet<>();
        DoubleSummaryStatistics statistics = transactions.stream()
                //filter account`s  transaction
                .filter(t ->
                        t.getFromAccountId().equals(accountId) || t.getToAccountId().equals(accountId))
                //extract reversed transaction
                .filter(t -> {
                    if(t.getTransactionType() == TransactionType.REVERSAL) {
                        reversedTransactions.add(t.getRelatedTransaction());
                        return false;
                    }
                    else {
                        return true;
                    }
                    })
                //filter transaction falling in date range and not reversed
                .filter(t -> t.getCreatedAt().isAfter(from) && t.getCreatedAt().isBefore(to)
                        && !reversedTransactions.contains(t.getTransactionId()))
                .mapToDouble(t -> {
                    if(t.getFromAccountId().equals(accountId))
                        return -t.getAmount();
                    else
                        return t.getAmount();
                })
                .summaryStatistics();
        
        return  statistics;
    }
}
