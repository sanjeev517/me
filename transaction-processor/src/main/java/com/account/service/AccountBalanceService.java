package com.account.service;

import java.time.LocalDateTime;
import java.util.DoubleSummaryStatistics;

public interface AccountBalanceService {

    DoubleSummaryStatistics calculateRelativeBalance(String accountId, LocalDateTime from , LocalDateTime to );
}
