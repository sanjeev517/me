package com.account.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.account.model.Transaction;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class TransactionDao {

	private List<Transaction> transactions;
	public TransactionDao(InputStream is) {
		CsvMapper csvMapper = new CsvMapper();
		csvMapper.registerModule(new JavaTimeModule());
		
		CsvSchema schema = CsvSchema.emptySchema().withHeader();
		try {
			MappingIterator<Transaction> iterator = csvMapper.readerFor(Transaction.class).with(schema)
					.readValues(is);
			transactions = iterator.readAll();
		} catch (IOException e) {
			throw new RuntimeException("Failed to parse file", e);
		}
	}

	public List<Transaction> getAllTransactions() {
		return transactions;
	}
}
