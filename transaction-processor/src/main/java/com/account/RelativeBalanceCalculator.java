package com.account;

import java.io.InputStream;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.DoubleSummaryStatistics;
import java.util.Scanner;

import com.account.data.TransactionDao;
import com.account.model.Transaction;
import com.account.service.AccountBalanceService;
import com.account.service.AccountBalanceServiceImpl;

public class RelativeBalanceCalculator {
    public static void main(String[] args) throws URISyntaxException {
    	RelativeBalanceCalculator calculator = new RelativeBalanceCalculator();
    	calculator.calculate();
    }

    private void calculate() throws URISyntaxException{
    	InputStream is = getClass().getClassLoader().getResourceAsStream("transactions.csv");
    	TransactionDao trxDao =  new TransactionDao(is);
        
        AccountBalanceService accountBalanceService = new AccountBalanceServiceImpl(trxDao);

        Scanner scan = new Scanner(System.in);

        System.out.println("accountId : ");
        String accountId = scan.nextLine().trim();
        System.out.println("From(dd/MM/yyyy HH:mm:ss) : ");
        String from = scan.nextLine().trim();
        System.out.println("To(dd/MM/yyyy HH:mm:ss) : ");
        String to = scan.nextLine().trim();

        DoubleSummaryStatistics output = accountBalanceService.calculateRelativeBalance(accountId, LocalDateTime.parse(from, Transaction.FORMATTER), LocalDateTime.parse(to, Transaction.FORMATTER));

        System.out.println("Relative balance for the period is :" + output.getSum());
        System.out.println("Number of transactions included is :" + output.getCount());


        scan.close();
    }

}
